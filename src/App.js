import React from 'react'
import './assets/css/styles.css'
import HomePage from './components/Home/HomePage'

function App() {
  return (
    <div>
      <HomePage/>
    </div>
  );
}

export default App;
