// son funciones que devuelven los tipos de acciones
import axios from 'axios'
import * as types from './action-type'

export const crtGetTopHeadlines = () => {
    // console.log(process.env.REACT_APP_TOP_HEADLINES_URL + process.env.REACT_APP_API_KEY)    
    return function(dispatch){
        // Metodo con fetch
        // fetch(process.env.REACT_APP_TOP_HEADLINES_URL + process.env.REACT_APP_API_KEY)
        //     .then(r => r.json())
        //     .then(r => {dispatch(
        //         {
        //             type: types.ACT_GET_TOP_HEADLINES_URL,
        //             topHeadlines: r
        //         }
        //     )})

        // metodo con axios
        axios.get(process.env.REACT_APP_TOP_HEADLINES_URL + process.env.REACT_APP_API_KEY)
            .then(r => {
                dispatch(
                    {
                        type: types.ACT_GET_TOP_HEADLINES_URL,
                        topHeadlines: r.data
                    }
                )
            })
    }


    // return{
    //     type: types.ACT_GET_TOP_HEADLINES_URL,
    //     topHeadlines: []
    // }
}
