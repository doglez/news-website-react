/* eslint-disable no-unused-vars */
import React, { useState } from 'react'
import { connect } from 'react-redux'
import News from '../News/News'
import * as creators from '../../actions/action-creator'

const HomePage = (props) => {

    const [data, setData] = useState(props.getTopHeadlines)
    
    return (
        <div className='wrapper'>
            <News news={props.topHeadlines}/>
        </div>
    )
}

const mapStateToProps = (state) => {   
    // console.log(state.totalResults) 
    return{
        topHeadlines: state.articles
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getTopHeadlines : () => {
            dispatch(creators.crtGetTopHeadlines())
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(HomePage)