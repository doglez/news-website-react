import React from 'react'
import NewsItems from './NewsItems'

const News = (props) => {
    // console.log(props.news)
    
    return (
        <div className='cards'>
          
          {props.news.map(newArticle => {
            // console.log(newArticle.title)
            return(
              <NewsItems newArticle={newArticle} key={newArticle.title}/>
            )
          })}

        </div>
    )
}

export default News
