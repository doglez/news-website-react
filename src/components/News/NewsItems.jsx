import React from 'react'

const NewsItems = (props) => {
    return (
        <div className='card'>
            <div className='news-items card-content'>
                <img src={props.newArticle.urlToImage} alt={props.newArticle.title} className='img-news'/>
                <h2>{props.newArticle.title}</h2>
                <h3>Autor: {props.newArticle.author}</h3>
                <h5>{props.newArticle.publishedAt}</h5>
                <p>{props.newArticle.content}</p>
                <a href={props.newArticle.url} target="_blank" rel="noopener noreferrer">ver mas</a>
            </div>
        </div>
    )
}

export default NewsItems
