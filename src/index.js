import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import theOnlySourceOfTruth from './store/store'
import App from './App';


ReactDOM.render(
  <React.StrictMode>
    <Provider store={theOnlySourceOfTruth}>
      <App />
    </Provider>    
  </React.StrictMode>,
  document.getElementById('root')
);
