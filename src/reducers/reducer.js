// es una funcion que recibe el estado actual y la accion, dependiendo la accion modifica el estado actual
import * as types from '../actions/action-type'

export const reducer = (state,action) =>{
    let newState = {...state}

    // aca va la logia del negocio
    // console.log('paso por el reducer', action.type)
    switch (action.type) {
        case types.ACT_GET_TOP_HEADLINES_URL:
            newState = action.topHeadlines
            break;
    
        default:
            break;
    }
    // console.log(newState)
    return newState
}