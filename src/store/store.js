import {reducer} from '../reducers/reducer'
import { applyMiddleware, createStore } from "redux"
import thunk from 'redux-thunk'


const defaultTopHeadlines = {
    "status": "ok",
    "totalResults": 3,
    "articles": []}


const theOnlySourceOfTruth = createStore(reducer, defaultTopHeadlines, applyMiddleware(thunk))

export default theOnlySourceOfTruth